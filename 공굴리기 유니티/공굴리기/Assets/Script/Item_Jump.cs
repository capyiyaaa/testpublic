﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Jump : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GameManager.instance.ChangeJumpabillity(1);

        gameObject.SetActive(false);
    }
}
