﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public GameObject yellowparticle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        GameManager.instance.ChangeScore(1);
        GameObject obj = Instantiate(yellowparticle);
        obj.transform.position = transform.position;
        gameObject.SetActive(false);
    }
}
