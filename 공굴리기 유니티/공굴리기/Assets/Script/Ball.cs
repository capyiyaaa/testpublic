﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    Rigidbody mRB;
    float speed = 50;
    public GameObject bullet;
    public GameObject target;
    public GameObject cameraPivot;
    public int isJump;
    public ParticleSystem mypart;
    // Start is called before the first frame update
    void Start()
    {
        mRB = GetComponent<Rigidbody>();
    }

    

    // Update is called once per frame
    void Update()
    {
        /*
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        */

        //Vector3 axis = new Vector3(horizontal, 0, vertical);

        //axis.y = mRB.velocity.y;

        //mRB.velocity = axis;
        //mRB.AddForce(axis);

        if (!GameManager.instance.mousevisible)
        {
            Vector3 horizontal = Input.GetAxisRaw("Horizontal") * cameraPivot.transform.right;
            Vector3 vertical = Input.GetAxisRaw("Vertical") * transform.forward;

            Vector3 velo = (horizontal + vertical).normalized;

            mRB.MovePosition(transform.position + velo * Time.deltaTime * speed * 0.3f);

            if (Input.GetButtonDown("Fire1"))
            {
                GameObject obj = Instantiate(bullet);
                obj.transform.rotation = transform.rotation;
                obj.transform.position = transform.position;
                obj.GetComponent<Bullet>().target = target.transform.position;
            }

            float myY = cameraPivot.transform.eulerAngles.x - Input.GetAxisRaw("Mouse Y");

            if (myY < 0)
            {
                myY = 0;
            }
            else if(myY > 20)
            {
                myY = 20;
            }
            //Y0일때, 5
            cameraPivot.transform.eulerAngles = new Vector3(myY, cameraPivot.transform.eulerAngles.y, 0);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + Input.GetAxisRaw("Mouse X") * 3f, transform.eulerAngles.z);
            //print(isJump);
            //print(GameManager.instance.jumpcount);

            if (isJump < GameManager.instance.jumpcount)
            {
                //print("can");
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    BallJump(1000);
                }
            }
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = 100;
        }
        else
        {
            speed = 50;
        }


    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "floor")
        {
            isJump = 0;
        }

    }

    public void BallJump(int value)
    {
        isJump++;
        mRB.velocity = new Vector3(mRB.velocity.x, 0, mRB.velocity.z);
        mRB.angularVelocity = new Vector3(mRB.velocity.x, 0, mRB.velocity.z);
        mRB.AddForce(new Vector3(0, value * GameManager.instance.jumpvalue, 0));
        mypart.Play();
    }

}
