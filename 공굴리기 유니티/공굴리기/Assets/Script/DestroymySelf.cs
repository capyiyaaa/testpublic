﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroymySelf : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyMySelf", 1);
    }

    void DestroyMySelf()
    {
        Destroy(gameObject);
    }
}
