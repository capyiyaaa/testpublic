﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Rigidbody mRB;
    public Vector3 target;
    // Start is called before the first frame update
    void Start()
    {
        mRB = GetComponent<Rigidbody>();
        Invoke("Destroyself", 2);
    }

    // Update is called once per frame
    void Update()
    {
        mRB.AddForce(target - transform.position*0.2f);
    }

    void Destroyself()
    {
        Destroy(gameObject);
    }
}
