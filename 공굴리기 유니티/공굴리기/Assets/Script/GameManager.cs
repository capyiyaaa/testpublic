﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int score;
    public Text txt_score;
    public Text txt_Abillity;
    public int jumpcount;
    public float jumpvalue=1;
    public int jumpabillity;
    string[] abillity;

    public static GameManager instance;
    private void Awake()
    {
        instance = this;
        Application.targetFrameRate = 60;

    }
    // Start is called before the first frame update
    void Start()
    {
        jumpvalue = 1;
        abillity = new string[] {"점프 아이템을 찾아봐요!", "점프 가능!", "더블점프 가능!", "트리플점프 가능!", "강화점프 가능!" };
        ChangeJumpabillity(0);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = mousevisible;
    }

    public bool mousevisible;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            mousevisible = !mousevisible;
            Cursorvis(mousevisible);
        }
    }


    void Cursorvis(bool check)
    {
        if (check)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = check;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = check;
        }
    }

    public void ChangeScore(int addscore)
    {
        score += addscore;
        txt_score.text = score.ToString();
    }

    public void ChangeJumpabillity(int addjump)
    {
        jumpabillity += addjump;
        txt_Abillity.text = abillity[jumpabillity];



        if(jumpabillity > 3)
        {
            jumpvalue = 2;
        }
        else if(addjump > 0)
        {
            print("jumpcount");
            jumpcount++;
        }
    }
}
